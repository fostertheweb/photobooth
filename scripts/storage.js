'use strict';

var Storage = (function(){

    var photos;
    var error = null;

    // load photos from local storage
    function loadFilmstrip(){
        if (window.localStorage){
            if (!localStorage.photos){
                photos = "none";
            } else {
                photos = JSON.parse(localStorage.photos);
                Webcam.load(photos);
            }
        } else {
            error = "Local Storage not supported. Save any photos you take."
        }
    }

    function saveFilmstrip(photoURLs){
        // get current filmstrap
        var urls = [];
        photoURLs.forEach(function(url, index){
            urls.push(url);
        });

        photos = urls;
        
        // try to save photos, show error if localStorage is full
        try {
            localStorage.photos = JSON.stringify(urls);
        } catch(e) {
            console.log(e);
            if (e == QUOTA_EXCEEDED_ERR) {
                error = "Storage space exceeded. Please delete photos from the film strip.";
            }
        }
    }

    // remove all photos from localStorage
    function clearFilmstrip(){
        localStorage.removeItem('photos');
    }

    // remove single photo from storage
    function removePhoto(photoURL){
        photos.forEach(function(element, index){
            if (element == photoURL){
                photos.splice(index, 1);
            }
        });

        localStorage.photos = JSON.stringify(photos);
    }

    return {
        load: loadFilmstrip,
        save: saveFilmstrip,
        clear: clearFilmstrip,
        remove: removePhoto,
        error: error
    };

})();
