/*

Learned how to implement these effects here:
http://www.storminthecastle.com/2013/04/06/how-you-can-do-cool-image-effects-using-html5-canvas/

*/

'use strict';

var Effect = (function(){

	var photoEffect;
	var view = document.getElementById('view');
	var context = view.getContext('2d');

	// update the canvas
	function update(){
		context.clearRect(0, 0, view.width, view.height);
		context.drawImage(normalImage, 0, 0, view.width, view.height);
		addEffect(photoEffect);
	}

	// apply effect to photo on canvas
	function addEffect(effect){
		var imageData = context.getImageData(0, 0, view.width, view.height);
		effect(imageData);
		context.putImageData(imageData, 0, 0);
	}

	// set the current effect
	function set(effect){
		photoEffect = effect;
		update();
	}

	// no effect
	function normal(){
		context.drawImage(normalImage, 0, 0, view.width, view.height);
	}

	// black and white
	function grayscale(pixels){
		var d = pixels.data;
		for (var i = 0; i < d.length; i += 4) {
			var r = d[i];
			var g = d[i + 1];
			var b = d[i + 2];
			d[i] = d[i + 1] = d[i + 2] = (r+g+b)/3;
		}

		return pixels;
	}

	// sepia
	function sepia(pixels){
		var d = pixels.data;
		for (var i = 0; i < d.length; i += 4) {
			var r = d[i];
			var g = d[i + 1];
			var b = d[i + 2];
			d[i]     = (r * 0.393)+(g * 0.769)+(b * 0.189); // red
			d[i + 1] = (r * 0.349)+(g * 0.686)+(b * 0.168); // green
			d[i + 2] = (r * 0.272)+(g * 0.534)+(b * 0.131); // blue
		}

		return pixels;
	}

	// red tint
	function red(pixels){
		var d = pixels.data;
		for (var i = 0; i < d.length; i += 4) {
			var r = d[i];
			var g = d[i + 1];
			var b = d[i + 2];
			d[i] = (r+g+b)/3;
			d[i + 1] = d[i + 2] = 0;
		}

		return pixels;
	}

	// green tint
	function green(pixels){
		var d = pixels.data;
		for (var i = 0; i < d.length; i += 4) {
			var r = d[i];
			var g = d[i + 1];
			var b = d[i + 2];
			d[i + 1] = (r+g+b)/3;
			d[i] = d[i + 2] = 0;
		}

		return pixels;
	}

	// blue tint
	function blue(pixels){
		var d = pixels.data;
		for (var i = 0; i < d.length; i += 4) {
			var r = d[i];
			var g = d[i + 1];
			var b = d[i + 2];
			d[i + 2] = (r+g+b)/3;
			d[i] = d[i + 1] = 0;
		}

		return pixels;
	}

	return {
		set: set,
		none: normal,
		grayscale: grayscale,
		sepia: sepia,
		red: red,
		green: green,
		blue: blue
	};
})();