'use strict';

var video = document.querySelector('video');
var source = document.getElementById('source').getContext('2d');
var normalImage;

// start webcam
Webcam.init();

// load photos
Storage.load();

// set up canvas
var setupCanvas = function(){
  if (video.videoWidth) {
    source.canvas.width = video.videoWidth;
    source.canvas.height = video.videoHeight;
  }
  draw();
};

// set up canvas after <video> loads
video.addEventListener('loadedmetadata', setupCanvas);

var cameraContainer = document.querySelector('.camera');
var info = document.querySelector('.info');

// check for user media support
if(Webcam.userMediaSupport){
  info.style.display = 'none';
  cameraContainer.classList.add('supported');
} else {
  info.textContent = 'Error accessing webcam.';
  info.style.display = 'block';
  info.classList.add('error');
}

// check for localStorage errors
if(!Storage.error){
  info.style.display = 'none';
} else {
  info.textContent = Storage.error;
  info.style.display = 'block';
  info.classList.add('error');
}

// take photo with button
var takePhotoButton = document.getElementById('takePhotoButton');
takePhotoButton.addEventListener('click', Webcam.takePhoto);

// take photo with spacebar
window.addEventListener('keyup', function(event){
  if (event.keyCode === 32){
    Webcam.takePhoto();
  }
});

// save current image
var editedPhoto = document.getElementById('view');
var savePhotoButton = document.getElementById('savePhotoButton');

savePhotoButton.addEventListener('click', function(){
  savePhotoButton.href = editedPhoto.toDataURL();
  if ('download' in savePhotoButton){
    savePhotoButton.download = 'photo.png';
  }
});

// remove all photos from filmstrip and storage
var clearFilmstripButton = document.getElementById('clearFilmstripButton');
clearFilmstripButton.addEventListener('click', Webcam.clear);

// choose effect, and set the current effect
var effectSelect = document.getElementById('effects');
effectSelect.addEventListener('change', function(e){
  switch(e.target.value) {
    case 'normal':
      Effect.set(Effect.none);
      break;
    case 'grayscale':
      Effect.set(Effect.grayscale);
      break;
    case 'sepia':
      Effect.set(Effect.sepia);
      break;
    case 'red':
      Effect.set(Effect.red);
      break;
    case 'green':
      Effect.set(Effect.green);
      break;
    case 'blue':
      Effect.set(Effect.blue);
      break;
  }
});

// return interface to normal
var doneEditingButton = document.getElementById('doneButton');
doneEditingButton.addEventListener('click', function(){
  document.getElementById('view').style.display = 'none';
  document.getElementById('source').style.display = 'block';

  document.querySelector('.edit-controls').style.display = 'none';
  document.querySelector('.photo-controls').style.display = 'block';
});

// draw image to canvas
function draw() {
  requestAnimationFrame(draw);
  source.drawImage(video, 0, 0, video.videoWidth, video.videoHeight, 0, 0, source.canvas.width, source.canvas.height);
}
