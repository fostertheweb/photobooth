'use strict';

var Webcam = (function(){

    var supported = true;
    var photos = [];

    // set up user media
    function init() {
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        if (navigator.getUserMedia) {
            navigator.getUserMedia({ video: true }, userMediaSuccess, userMediaError);
            supported = true;
        }
    }

    // handle user media
    function userMediaSuccess(stream) {
        if ('mozSrcObject' in video) {
            video.mozSrcObject = stream;
        } else if (window.webkitURL) {
            video.src = window.webkitURL.createObjectURL(stream);
        } else {
            video.src = stream;
        }

        video.play();
    }

    // handle user media error
    function userMediaError(error) {
        console.log('User Media Error: ', error.message);
        supported = false;
    }

    // create thumbnail with buttons
    function buildPhotoElement(dataURL){
        var filmstrip = document.querySelector('.filmstrip');
        var thumbnailContainer = document.createElement('div');
        var buttonContainer = document.createElement('div');
        var thumbnail = document.createElement('img');

        buttonContainer.classList.add('button-container');
        thumbnailContainer.classList.add('thumbnail-container');

        var editButton = document.createElement('a');
        var deleteButton = document.createElement('a');

        editButton.innerHTML = "Edit";
        editButton.classList.add('edit-button');

        deleteButton.innerHTML = "Delete";
        deleteButton.classList.add('delete-button');

        thumbnail.src = dataURL;

        buttonContainer.appendChild(editButton);
        buttonContainer.appendChild(deleteButton);

        thumbnailContainer.appendChild(buttonContainer);
        thumbnailContainer.appendChild(thumbnail);
        

        filmstrip.appendChild(thumbnailContainer);

        editButton.addEventListener('click', editPhoto);
        deleteButton.addEventListener('click', deletePhoto);
    }

    // save dataURLs to filmstrip/localStorage
    function storePhotos(){
        NodeList.prototype.forEach = HTMLCollection.prototype.forEach = Array.prototype.forEach;

        var photos = document.querySelectorAll('.filmstrip img');
        var photoURLs = [];

        photos.forEach(function(element, index){
            photoURLs.push(element.src);
        });

        Storage.save(photoURLs);
    }

    // take a photo
    function takePhoto(){
        var dataURL = document.getElementById('source').toDataURL();
        buildPhotoElement(dataURL);
        storePhotos();
    }

    // populate filmstrip
    function loadPhotos(photos){
        photos.forEach(function(url){
            buildPhotoElement(url);
        });
    }

    // remove all thumbnails
    function clearFilmstrip(){
        var filmstrip = document.querySelector('.filmstrip');
        while(filmstrip.firstChild) filmstrip.removeChild(filmstrip.firstChild);
        Storage.clear();
    }

    // change interface, view single photo
    function editPhoto(e){
        var container = e.target.parentNode.parentNode;
        var photo = container.querySelector('img');
        normalImage = photo;

        var editCanvas = document.getElementById('view');
        var view = editCanvas.getContext('2d');
        view.canvas.width = source.canvas.width;
        view.canvas.height = source.canvas.height;

        source.canvas.style.display = 'none';
        editCanvas.style.display = 'block';

        document.querySelector('.photo-controls').style.display = 'none';
        document.querySelector('.edit-controls').style.display = 'block';

        view.drawImage(photo, 0, 0, view.canvas.width, view.canvas.height);
    }

    // remove single photo from filmstrip
    function deletePhoto(e){
        var filmstrip = document.querySelector('.filmstrip');
        var container = e.target.parentNode.parentNode;
        var photo = container.querySelector('img').src;

        filmstrip.removeChild(container);
        Storage.remove(photo);
    }

    return {
        init: init,
        userMediaSupport: supported,
        takePhoto: takePhoto,
        photos: photos,
        load: loadPhotos,
        clear: clearFilmstrip
    };
})();
